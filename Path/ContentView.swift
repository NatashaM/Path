//
//  ContentView.swift
//  Path
//
//  Created by Наталья Миронова on 10.08.2023.
//

import SwiftUI
	
struct ContentView: View {
	@State private var isAnimation = false
	
	var body: some View {
		VStack(spacing: 70) {
			ZStack {
				Path { path in
					path.move(to: CGPoint(x: 100, y: 50))
					path.addQuadCurve(
						to: CGPoint(x: 200, y: 100),
						control: CGPoint(x: 170, y: 0)
					)
					path.addQuadCurve(
						to: CGPoint(x: 100, y: 200),
						control: CGPoint(x: 150, y: 150)
					)
					path.addQuadCurve(
						to: CGPoint(x: 0, y: 100),
						control: CGPoint(x: 0, y: 100)
					)
					path.addQuadCurve(
						to: CGPoint(x: 100, y: 50),
						control: CGPoint(x: 30, y: 0)
					)
				}
				.fill(RadialGradient(colors: [.white, .pink],
									 center: .center,
									 startRadius: 5,
									 endRadius: 90))
				.frame(width: 200, height: 200)
				.scaleEffect(isAnimation ? 1.5 : 1.0)
				.animation(.easeInOut, value: isAnimation)
				
				if isAnimation {
					Text("❤️")
						.font(.system(size: 60))
				}
			}
			
			Button(action: { isAnimation.toggle() }) {
				Text("Нажми")
					.font(.largeTitle)
					.foregroundColor(.white)
			}
			.background(Color.pink)
			.cornerRadius(10)
		}
	}
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
			.frame(width: 200, height: 200)
    }
}
