//
//  PathApp.swift
//  Path
//
//  Created by Наталья Миронова on 10.08.2023.
//

import SwiftUI

@main
struct PathApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
